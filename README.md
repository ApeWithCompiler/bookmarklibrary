![logo](doc/logo_full.png)

<br>

A small terminal application for managing your browser bookmarks

[![pipeline status](https://gitlab.com/ApeWithCompiler/bookmarklibrary/badges/master/pipeline.svg)](https://gitlab.com/ApeWithCompiler/bookmarklibrary/-/commits/master)

## What is this for?
It is a CLI application for managing your bookmarks independend from the browser you are using. Also you can split up your bookmarks in seperate databases and split private and professional interests.

## Disclaimer

Many lines of the code may seem very unclean. This was because I needed the application wether it was well coded or not.


## Dependencies
Currently the app uses follwing dependencies:
* github.com/c-bata/go-prompt
* github.com/go-gorm/gorm
* github.com/spf13/cobra
* golang.org/x/net

## Build

To build the tool run:
``` sh
make build
```

It will create a ***bin/*** folder. It then creates the go.sum file. At last the dependencies will be downloaded and the application compiled. It can then be found in ***bin/bookmarks***.

For further information see the ***Makefile***.


## Usage

For information on the usage refer to the [wiki](https://gitlab.com/ApeWithCompiler/bookmarklibrary/-/wikis/Usage).