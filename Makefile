CI_COMMIT_SHORT_SHA := $(shell git rev-parse --short HEAD)
CI_COMMIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)

build:
	mkdir -p bin
	go mod tidy
	go build -ldflags "-X gitlab.com/ApeWithCompiler/bookmarklibrary/internal/version.AppVersion=1.0 -X gitlab.com/ApeWithCompiler/bookmarklibrary/internal/version.GitCommit=$(CI_COMMIT_SHORT_SHA) -X gitlab.com/ApeWithCompiler/bookmarklibrary/internal/version.GitBranch=$(CI_COMMIT_BRANCH)" -o bin/bookmarks main.go