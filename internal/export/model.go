package export

/*
Entities for internal usage
*/

type Tag struct {
	Id   int
	Name string
}

type Link struct {
	Id   int
	Url  string
	Name string
	Tags []Tag
}

type Library struct {
	Links []Link
}

/*
Entities for import/export to json
*/

type TransferLink struct {
	Url  string   `json:"url"`
	Name string   `json:"name"`
	Tags []string `json:"tags"`
}

type TransferLibrary struct {
	Links []TransferLink `json:"links"`
}

type LinkRequestBody struct {
	Links []TransferLink `json:"data"`
}

func CreateTag(name string) Tag {
	return Tag{
		Name: name,
	}
}

func CreateLink(name string, url string, tags []string) Link {
	link := Link{
		Url:  url,
		Name: name,
	}

	for _, tag := range tags {
		link.Tags = append(link.Tags, CreateTag(tag))
	}

	return link
}
