package export

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// SaveFile represents a container for a file that is exported or imported.
type SaveFile struct {
	Path string
}

// Read reads a file and returns a model of its library.
func (file *SaveFile) Read() TransferLibrary {
	jsonFile, err := os.Open(file.Path)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	var library TransferLibrary
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &library)

	return library
}

// Write writes a model of the library to a file.
func (file *SaveFile) Write(library TransferLibrary) {
	byteValue, _ := json.Marshal(library)
	ioutil.WriteFile(file.Path, byteValue, os.ModePerm)
}
