package config

import (
	"os/user"
	"path/filepath"
)

// GetDefaultLibraryPath returns a path containing the "default library" sqlite file.
func GetDefaultLibraryPath() string {
	usr, _ := user.Current()
	dir := usr.HomeDir

	path := filepath.Join(dir, "Documents/bookmarks.bl")
	return path
}
