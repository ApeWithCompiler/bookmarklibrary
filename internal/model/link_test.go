package model

import (
	"testing"
)

func TestLink_CreateLink(t *testing.T) {
	expected := Link{Url: "foo", Name: "bar"}
	result := CreateLink("foo", "bar")

	// Can't reflect because of gorms auto generated timestamps
	if result.Url != expected.Url || result.Name != expected.Name {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestLink_SetName(t *testing.T) {
	expected := "baz"
	result := Link{Url: "foo", Name: "bar"}
	result.SetName("baz")

	if result.Name != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestLink_SetUrl(t *testing.T) {
	expected := "baz"
	result := Link{Url: "foo", Name: "bar"}
	result.SetUrl("baz")

	if result.Url != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}
