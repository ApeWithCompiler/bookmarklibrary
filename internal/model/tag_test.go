package model

import (
	"testing"
)

func TestTag_CreateTag(t *testing.T) {
	expected := Tag{Name: "foo"}
	result := CreateTag("foo")

	// Can't reflect because of gorms auto generated timestamps
	if result.Name != expected.Name {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}
