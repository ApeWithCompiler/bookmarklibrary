package model

import (
	"gorm.io/gorm"
)

// UserTag implements a tag associated with a Link.
type Tag struct {
	gorm.Model
	ID     uint   `gorm:"primaryKey"`
	Name   string `gorm:"index"`
	LinkId uint   `gorm:"index"`
}

// CreateUserTag returns a new UserTag instance.
// Mostly for convenience.
func CreateTag(name string) Tag {
	return Tag{
		Name: name,
	}
}
