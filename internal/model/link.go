package model

import (
	"gorm.io/gorm"
)

// Link implements the model of a link, managed by this tool.
type Link struct {
	gorm.Model
	ID   uint   `gorm:"primaryKey"`
	Url  string `gorm:"unique"`
	Name string
	Tags []Tag `gorm:"foreignKey:LinkId"`
}

// CreateLink returns a new Link instance.
// Mostly for convenience.
func CreateLink(url, name string) Link {
	return Link{
		Url:  url,
		Name: name,
	}
}

// SetName sets the Name attribute.
// Ment for not directly accessing the fields of the struct.
func (this *Link) SetName(Name string) {
	this.Name = Name
}

// SetName sets the Url attribute.
// Ment for not directly accessing the fields of the struct.
func (this *Link) SetUrl(url string) {
	this.Url = url
}
