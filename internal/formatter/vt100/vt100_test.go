package vt100

import (
	"testing"
)

func TestVt100_CursorPos(t *testing.T) {
	expected := "\033[10;20H"
	result := Cursorpos(10, 20)

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_CursorposHorizontal(t *testing.T) {
	expected := "\033[10G"
	result := CursorposHorizontal(10)

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_CursorposVertical(t *testing.T) {
	expected := "\033[10d"
	result := CursorposVertical(10)

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_Cursorrt(t *testing.T) {
	expected := "\033[10C"
	result := Cursorrt(10)

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_Savecursor(t *testing.T) {
	expected := "\033[s"
	result := Savecursor()

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_Restorecursor(t *testing.T) {
	expected := "\033[u"
	result := Restorecursor()

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}

func TestVt100_Setattributemode(t *testing.T) {
	expected := "\033[0;31m"
	result := Setattributemode(31)

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}
