package vt100

import (
	"fmt"
)

const (
	NC      = 0
	Black   = 30
	Red     = 31
	Green   = 32
	Yellow  = 33
	Blue    = 34
	Magenta = 35
	Cyan    = 36
	White   = 37
)

func Cursorpos(row, col int) string {
	return fmt.Sprintf("\033[%v;%vH", row, col)
}

func CursorposHorizontal(col int) string {
	return fmt.Sprintf("\033[%vG", col)
}

func CursorposVertical(row int) string {
	return fmt.Sprintf("\033[%vd", row)
}

func Cursorrt(col int) string {
	return fmt.Sprintf("\033[%vC", col)
}

func Savecursor() string {
	return "\033[s"
}

func Restorecursor() string {
	return "\033[u"
}

func Setattributemode(attr interface{}) string {
	return fmt.Sprintf("\033[0;%vm", attr)
}
