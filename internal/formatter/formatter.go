package formatter

import (
	"fmt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter/vt100"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"sort"
	"strings"
)

// reduce Tags formats a slice of Tag to a slice of strings for easier printing.
func ReduceTags(tags []model.Tag) []string {
	var result []string
	for _, tag := range tags {
		result = append(result, tag.Name)
	}
	return result
}

func SetColor(color int, value interface{}) string {
	return fmt.Sprintf("%v%v%v", vt100.Setattributemode(color), value, vt100.Setattributemode(vt100.NC))
}

func HorizontalIndent(col int, value interface{}) string {
	return fmt.Sprintf("%v%v%v", vt100.CursorposHorizontal(0), vt100.Cursorrt(col), value)
}

// LinkMessage formats a string slice out of a Link structure.
func LinkMessage(link model.Link) []string {
	tags := ReduceTags(link.Tags)
	sort.Strings(tags)

	message := []string{
		SetColor(vt100.Cyan, "Id:"+HorizontalIndent(8, link.ID)),
		"Name:" + HorizontalIndent(8, link.Name),
		"Url:" + HorizontalIndent(8, link.Url),
		"Tags:" + HorizontalIndent(8, tags),
	}

	return message
}

// LinkMessageList formats a string slice of a Link structure slice.
func LinkMessageList(links []model.Link) []string {
	var message []string

	for i, _ := range links {
		block := LinkMessage(links[i])
		message = append(message, block...)
		message = append(message, "")
	}

	return message
}

// PromtLines prints multiple lines to the terminal.
func PromptLines(lines []string) {
	for i, _ := range lines {
		PromptLine(lines[i])
	}
}

// PromtLine prints a single line to the terminal.
func PromptLine(line string, a ...interface{}) {
	fmt.Printf(line, a...)
	fmt.Printf("\n")
}

func GroupByFirstLetter(lines []string) map[string][]string {
	result := map[string][]string{}
	for _, l := range lines {
		if len(l) > 0 {
			firstCharacter := strings.ToUpper(l[0:1])
			if _, ok := result[firstCharacter]; ok {
				result[firstCharacter] = append(result[firstCharacter], l)
			} else {
				result[firstCharacter] = []string{l}
			}
		}
	}

	return result
}

func PromptGoupMap(group map[string][]string) {
	for key, element := range group {
		PromptLine(fmt.Sprintf("%v:", key))
		for _, e := range element {
			PromptLine(fmt.Sprintf("\t%v", e))
		}
	}
}

func PromptGoupMapAlphabetically(group map[string][]string) {
	var keys []string
	for key, _ := range group {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		PromptLine("%v:", key)
		for _, e := range group[key] {
			PromptLine(HorizontalIndent(2, e))
		}
	}
}
