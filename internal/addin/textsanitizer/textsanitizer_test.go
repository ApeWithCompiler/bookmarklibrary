package textsanitizer

import (
	"testing"
)

func TestSanitizer_NewlinesSpacesCleared(t *testing.T) {
	expected := "test Input"
	result := Sanitize("\n\ntest Input\n")

	if result != expected {
		t.Errorf("Output incorrect, got %v, expect: %v", result, expected)
	}
}
