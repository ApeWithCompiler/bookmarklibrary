package textsanitizer

import (
	"strings"
)

func Sanitize(text string) string {
	text = strings.TrimSuffix(text, "\n")
	text = strings.TrimPrefix(text, "\n")

	text = strings.TrimSpace(text)

	return text
}
