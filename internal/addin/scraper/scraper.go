package scraper

import (
	"bytes"
	"errors"
	"fmt"
	"golang.org/x/net/html"
	"io/ioutil"
	"net/http"
)

// Page is a container, representing an html document.
type Page struct {
	body []byte
	url  string
}

// ScrapePage makes an http request and returns the page as a 'Page' struct.
func ScrapePage(url string) Page {
	res, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
	}

	body, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		fmt.Println(err)
	}

	return Page{
		url:  url,
		body: body,
	}
}

// GetTitle returns the <title> of the page.
func (this *Page) GetTitle() string {
	title, _ := this.getNode("title")
	return title.FirstChild.Data
}

// getNode recursivly traverses the html doc to search vor the node provided as 'name'.
func (this *Page) getNode(name string) (*html.Node, error) {
	doc, _ := html.Parse(bytes.NewReader(this.body))
	var body *html.Node
	var crawler func(*html.Node)
	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == name {
			body = node
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(doc)
	if body != nil {
		return body, nil
	}
	return nil, errors.New("Missing " + name + " in the node tree")
}
