package browser

import (
	"fmt"
	"os/exec"
	"runtime"
)

// OpenInDefaultBrowser opens a url in the hosts configured default browser.
func OpenInDefaultBrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("ERROR: Can't open url, invalid OS. Supported OS are: linux, windows, darwin")
	}
	if err != nil {
		fmt.Println(err)
	}

}
