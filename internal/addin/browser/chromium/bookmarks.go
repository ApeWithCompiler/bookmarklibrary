package chromium

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"

	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
)

type BookmarksFile struct {
	Roots Roots `json:"roots"`
}

type Roots struct {
	BookmarkBar Node `json:"bookmark_bar"`
	Synced Node `json:"synced"`
	Other Node `json:"other"`
}

type Node struct {
	Name string `json:"name"`
	Type string `json:"type"`
	Url string `json:"url"`
	Children []Node `json:"children"`
}

// Read reads a file and returns a model of its content.
func Read(path string) BookmarksFile {
	jsonFile, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	var file BookmarksFile
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &file)

	return file
}

func GetLinuxDefaultFilePath() string {
	usr, _ := user.Current()
	dir := usr.HomeDir

	path := filepath.Join(dir, ".config/chromium/Default/Bookmarks")
	return path
}

func GetLinks(file BookmarksFile) []model.Link {
	var links []model.Link

	links = append(links, ParseNodeRecursive(file.Roots.BookmarkBar)...)
	links = append(links, ParseNodeRecursive(file.Roots.Synced)...)
	links = append(links, ParseNodeRecursive(file.Roots.Other)...)

	return links
}

func ParseNodeRecursive(node Node) []model.Link {
	var links []model.Link

	switch node.Type {
	case "folder":
		for _, c := range node.Children {
			l := ParseNodeRecursive(c)
			links = append(links, l...) 
		}
	case "url":
		l := model.CreateLink(node.Url, node.Name)
		links = append(links, l)
	}

	return links
}
