package completer

import (
	"github.com/c-bata/go-prompt"
)

var rootCmd = &Command{
	Name: "bookmarks",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		suggestions := []prompt.Suggest{
			{Text: "quit", Description: "Quit the app"},
			{Text: "exit", Description: "Quit the app"},
			{Text: "add", Description: "Add a link"},
			{Text: "delete", Description: "Delete links"},
			{Text: "get", Description: "Get resources"},
			{Text: "tag", Description: "Add tags to a link"},
			{Text: "untag", Description: "Remove tags from a link"},
			{Text: "browse", Description: "Browse links"},
			{Text: "edit", Description: "Edit links"},
			{Text: "search", Description: "Search for links"},
			{Text: "version", Description: "Print the version"},
			{Text: "import", Description: "Import a library"},
			{Text: "export", Description: "Export a library"},
			{Text: "migrate", Description: "Migrate the library"},
		}
		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}

func filterOptionSuggestions() []prompt.Suggest {
	return []prompt.Suggest{
		{Text: "--id", Description: "Specify link ids to select"},
		{Text: "--tag", Description: "Specify tags ids to select"},
		{Text: "--name", Description: "Specify names ids to select"},
		{Text: "--url", Description: "Specify urls ids to select"},
	}
}

func Init() {
	rootCmd.AddCommand(searchCmd)
	searchCmd.AddCommand(searchTagCmd)
	rootCmd.AddCommand(editCmd)
	rootCmd.AddCommand(getCmd)
	getCmd.AddCommand(getLinksCmd)
	getCmd.AddCommand(getTagsCmd)
	rootCmd.AddCommand(tagCmd)
	rootCmd.AddCommand(untagCmd)
	rootCmd.AddCommand(browseCmd)
	rootCmd.AddCommand(deleteCmd)
	rootCmd.AddCommand(addCmd)
}
