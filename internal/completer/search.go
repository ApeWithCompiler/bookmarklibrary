package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

func wildcardTerm(term string) string {
	return "%" + term + "%"
}

var searchCmd = &Command{
	Name: "search",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		var suggestions []prompt.Suggest
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		if GetParamBeforeCurrent(args) == "--tag" || GetParamBeforeCurrent(args) == "-t" {
			tags := linkr.ListTagsDistinct()
			suggestions = formatTagSuggestions(tags)
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if GetParamBeforeCurrent(args) == "--name" || GetParamBeforeCurrent(args) == "-n" {
			links := linkr.FindLinksLikeName(wildcardTerm(d.GetWordBeforeCursor()))

			for _, link := range links {
				suggestions = append(suggestions, prompt.Suggest{Text: link.Name})
			}
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if GetParamBeforeCurrent(args) == "--url" || GetParamBeforeCurrent(args) == "-u" {
			links := linkr.FindLinksLikeUrl(wildcardTerm(d.GetWordBeforeCursor()))

			for _, link := range links {
				suggestions = append(suggestions, prompt.Suggest{Text: link.Url})
			}
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		suggestions = []prompt.Suggest{
			{Text: "--wildcard", Description: "Use wildcard search"},
			{Text: "--tag", Description: "Search by tag"},
			{Text: "--name", Description: "Search by name"},
			{Text: "--url", Description: "Search by url"},
		}
		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}

var searchTagCmd = &Command{
	Name: "--tag",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		tags := linkr.ListTagsDistinct()

		return prompt.FilterHasPrefix(formatTagSuggestions(tags), d.GetWordBeforeCursor(), true)
	},
}
