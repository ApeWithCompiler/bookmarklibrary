package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"strconv"
)

func formatTagSuggestions(tags []model.Tag) []prompt.Suggest {
	var suggestions []prompt.Suggest
	for _, tag := range tags {
		suggestions = append(suggestions, prompt.Suggest{Text: tag.Name})
	}
	return suggestions
}

func formatLinkSuggestions(links []model.Link) []prompt.Suggest {
	var suggestions []prompt.Suggest
	for _, link := range links {
		suggestions = append(suggestions, prompt.Suggest{Text: strconv.Itoa(int(link.ID)), Description: link.Name})
	}
	return suggestions
}
