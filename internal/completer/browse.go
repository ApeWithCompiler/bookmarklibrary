package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"strconv"
)

var browseCmd = &Command{
	Name: "browse",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		var suggestions []prompt.Suggest
		if _, err := strconv.Atoi(d.GetWordBeforeCursor()); err == nil {
			links := linkr.ListLinks()
			suggestions = formatLinkSuggestions(links)
		} else {
			links := linkr.ListLinks()
			for _, link := range links {
				suggestions = append(suggestions, prompt.Suggest{Text: link.Name})
			}
		}
		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}
