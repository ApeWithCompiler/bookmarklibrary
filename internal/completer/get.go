package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var getCmd = &Command{
	Name: "get",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		suggestions := []prompt.Suggest{
			{Text: "links", Description: "Get all links stored in the library"},
			{Text: "tags", Description: "Get all distinced stored in the library"},
		}

		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}

var getLinksCmd = &Command{
	Name: "links",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		var suggestions []prompt.Suggest

		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		links := linkr.ListLinks()
		suggestions = formatLinkSuggestions(links)
		suggestions = append(suggestions, prompt.Suggest{Text: "--tail", Description: "Only show the last bookmarks"})

		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}

var getTagsCmd = &Command{
	Name: "tags",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		suggestions := []prompt.Suggest{
			{Text: "--pretty", Description: "rints grouped by the first letter"},
		}

		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}
