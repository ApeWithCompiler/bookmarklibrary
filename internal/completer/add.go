package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var addCmd = &Command{
	Name: "add",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		var suggestions []prompt.Suggest

		if GetParamBeforeCurrent(args) == "--tag" || GetParamBeforeCurrent(args) == "-t" {
			usertags := linkr.ListTagsDistinct()
			suggestions = formatTagSuggestions(usertags)
		}
		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}
