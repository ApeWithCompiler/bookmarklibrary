package completer

import (
	"encoding/csv"
	"github.com/c-bata/go-prompt"
	"strings"
)

func indexOf(list []Flag, element Flag) int {
	for i, v := range list {
		if v.Name == element.Name {
			return i
		}
	}
	return -1
}

// Framework for modeling suggestions
// In usage it is oriented on cobra
// Suggestions are modeled as commands for which they
// give the suggestions
// Execute shows if a subcommand is registered matching the first argument
// and then recursivly calls itself on the subcommand
// Otherwhise the Suggest of the current command will be called
type Flag struct {
	Name        string
	Description string
}

type Command struct {
	Name        string
	Flags       []Flag
	SubCommands map[string]*Command
	Suggest     func(d prompt.Document, args []string, opts Options) []prompt.Suggest
}

func (c *Command) Execute(d prompt.Document, args []string, opts Options) []prompt.Suggest {
	if c.SubCommands != nil {
		if len(args) >= 1 {
			token := args[0]
			if _, ok := c.SubCommands[token]; ok {
				if len(args) > 1 {
					args = args[1:]
				} else {
					args = args[:0]
				}
				command := c.SubCommands[token]
				return (*command).Execute(d, args, opts)
			}
		}
	}

	return c.Suggest(d, args, opts)
}

func (c *Command) AddCommand(subCommand *Command) {
	if c.SubCommands == nil {
		c.SubCommands = make(map[string]*Command)
	}

	c.SubCommands[subCommand.Name] = subCommand
}

func (c *Command) AddFlag(flag Flag) {
	if indexOf(c.Flags, flag) == -1 {
		c.Flags = append(c.Flags, flag)
	}
}

// Implementation of the completer itself

type Options struct {
	LibraryFile string
}

type Completer struct {
	RootCommand *Command
	Opts        Options
}

func CreateCompleter(libraryfile string) Completer {
	var completer Completer
	completer.Opts.LibraryFile = libraryfile
	completer.RootCommand = rootCmd
	Init()

	return completer
}

func (c *Completer) GetSuggestions(d prompt.Document) []prompt.Suggest {
	line := d.CurrentLine()
	tokens := splitLine(line)

	return (*c.RootCommand).Execute(d, tokens, c.Opts)
}

func splitLine(inputline string) []string {
	r := csv.NewReader(strings.NewReader(inputline))
	r.Comma = ' ' // space
	tokens, err := r.Read()
	if err != nil {
	}

	return tokens
}
