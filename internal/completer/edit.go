package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var editCmd = &Command{
	Name: "edit",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		var suggestions []prompt.Suggest

		if len(args) == 1 {
			links := linkr.ListLinks()
			suggestions = formatLinkSuggestions(links)
		} else {
			suggestions = []prompt.Suggest{
				{Text: "--name", Description: "Edit the name"},
				{Text: "--url", Description: "Edit the url"},
			}
		}

		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}
