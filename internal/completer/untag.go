package completer

import (
	"github.com/c-bata/go-prompt"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var untagCmd = &Command{
	Name: "untag",
	Suggest: func(d prompt.Document, args []string, opts Options) []prompt.Suggest {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(opts.LibraryFile)
		defer linkr.Close()

		var suggestions []prompt.Suggest

		if GetParamBeforeCurrent(args) == "--id" || GetParamBeforeCurrent(args) == "-i" {
			links := linkr.ListLinks()
			suggestions = formatLinkSuggestions(links)
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if GetParamBeforeCurrent(args) == "--tag" || GetParamBeforeCurrent(args) == "-t" {
			tags := linkr.ListTagsDistinct()
			suggestions = formatTagSuggestions(tags)
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if GetParamBeforeCurrent(args) == "--name" || GetParamBeforeCurrent(args) == "-n" {
			links := linkr.FindLinksLikeName(wildcardTerm(d.GetWordBeforeCursor()))

			for _, link := range links {
				suggestions = append(suggestions, prompt.Suggest{Text: link.Name})
			}
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if GetParamBeforeCurrent(args) == "--url" || GetParamBeforeCurrent(args) == "-u" {
			links := linkr.FindLinksLikeUrl(wildcardTerm(d.GetWordBeforeCursor()))

			for _, link := range links {
				suggestions = append(suggestions, prompt.Suggest{Text: link.Url})
			}
			return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
		}

		if len(args) == 1 {
			return prompt.FilterHasPrefix(filterOptionSuggestions(), d.GetWordBeforeCursor(), true)
		}

		tags := linkr.ListTagsDistinct()
		suggestions = formatTagSuggestions(tags)
		suggestions = append(suggestions, filterOptionSuggestions()...)

		return prompt.FilterHasPrefix(suggestions, d.GetWordBeforeCursor(), true)
	},
}
