package completer

import ()

func GetParamBeforeCurrent(args []string) string {
	if len(args) > 1 {
		return args[len(args)-2]
	} else {
		return ""
	}
}
