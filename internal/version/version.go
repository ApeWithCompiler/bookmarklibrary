package version

import ()

// Version information.
// This is meant to be set while compilation.
var AppVersion string
var GitBranch string
var GitCommit string
