package sqlite

import (
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
)

func ToWildcardTerm(term string) string {
	return "%" + term + "%"
}

func ToUnique(input []model.Link) []model.Link {
	var output []model.Link

	linksEncountered := map[uint]bool{}

	for _, link := range input {
		if linksEncountered[link.ID] != true {
			linksEncountered[link.ID] = true
			output = append(output, link)
		}
	}

	return output
}

type QueryExecuter interface {
	Execute() []model.Link
}


type IdQueryExecuter struct {
	reader LinkReader

	id int
}

func NewIdQueryExecuter(reader LinkReader, id int) *IdQueryExecuter {
	return &IdQueryExecuter{
		reader: reader,
		id: id,
	}
}

func (e *IdQueryExecuter) Execute() []model.Link {
	if e.reader.LinkExists(e.id) {
		return []model.Link{e.reader.GetLink(e.id),}
	}

	return []model.Link{}
}


type TagQueryExecuter struct {
	reader LinkReader

	tag string
}

func NewTagQueryExecuter(reader LinkReader, tag string) *TagQueryExecuter {
	return &TagQueryExecuter{
		reader: reader,
		tag: tag,
	}
}

func (e *TagQueryExecuter) Execute() []model.Link {
	return e.reader.FindLinksByTag(e.tag)
}


type NameQueryExecuter struct {
	reader LinkReader

	name string
}

func NewNameQueryExecuter(reader LinkReader, name string) *NameQueryExecuter {
	return &NameQueryExecuter{
		reader: reader,
		name: name,
	}
}

func (e *NameQueryExecuter) Execute() []model.Link {
	return e.reader.FindLinksByName(e.name)
}


type WildcardNameQueryExecuter struct {
	reader LinkReader

	name string
}

func NewWildcardNameQueryExecuter(reader LinkReader, name string) *WildcardNameQueryExecuter {
	return &WildcardNameQueryExecuter{
		reader: reader,
		name: name,
	}
}

func (e *WildcardNameQueryExecuter) Execute() []model.Link {
	return e.reader.FindLinksLikeName(ToWildcardTerm(e.name))
}


type UrlQueryExecuter struct {
	reader LinkReader

	url string
}

func NewUrlQueryExecuter(reader LinkReader, url string) *UrlQueryExecuter {
	return &UrlQueryExecuter{
		reader: reader,
		url: url,
	}
}

func (e *UrlQueryExecuter) Execute() []model.Link {
	return e.reader.FindLinksByUrl(e.url)
}


type WildcardUrlQueryExecuter struct {
	reader LinkReader

	url string
}

func NewWildcardUrlQueryExecuter(reader LinkReader, url string) *WildcardUrlQueryExecuter {
	return &WildcardUrlQueryExecuter{
		reader: reader,
		url: url,
	}
}

func (e *WildcardUrlQueryExecuter) Execute() []model.Link {
	return e.reader.FindLinksLikeUrl(ToWildcardTerm(e.url))
}


type QueryBuilder struct {
	reader LinkReader

	q []QueryExecuter	
}

func NewQueryBuilder(reader LinkReader) *QueryBuilder {
	return &QueryBuilder{
		reader: reader,
	}
}

func (b *QueryBuilder) WithTag(tag string) *QueryBuilder {
	b.q = append(b.q, NewTagQueryExecuter(b.reader, tag))
	return b
}

func (b *QueryBuilder) WithId(id int) *QueryBuilder {
	b.q = append(b.q, NewIdQueryExecuter(b.reader, id))
	return b
}

func (b *QueryBuilder) WithName(name string) *QueryBuilder {
	b.q = append(b.q, NewNameQueryExecuter(b.reader, name))
	return b
}

func (b *QueryBuilder) WithWildcardName(name string) *QueryBuilder {
	b.q = append(b.q, NewWildcardNameQueryExecuter(b.reader, name))
	return b
}

func (b *QueryBuilder) WithUrl(url string) *QueryBuilder {
	b.q = append(b.q, NewUrlQueryExecuter(b.reader, url))
	return b
}

func (b *QueryBuilder) WithWildcardUrl(url string) *QueryBuilder {
	b.q = append(b.q, NewWildcardUrlQueryExecuter(b.reader, url))
	return b
}

func (b *QueryBuilder) QueryUnique() []model.Link {
	var links []model.Link

	for _, executor := range b.q {
		links = append(links, executor.Execute()...)
	}

	return ToUnique(links)
}
