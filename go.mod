module gitlab.com/ApeWithCompiler/bookmarklibrary

go 1.14

require (
	github.com/c-bata/go-prompt v0.2.5
	github.com/spf13/cobra v1.1.3
	golang.org/x/net v0.0.0-20210415231046-e915ea6b2b7d
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.3
)
