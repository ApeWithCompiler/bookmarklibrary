package main

import (
	"gitlab.com/ApeWithCompiler/bookmarklibrary/cmd"
)

func main() {
	cmd.Init()
	cmd.Execute()
}
