package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/version"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print version",
	Long:  "Prints the version of the tool",
	Run: func(cmd *cobra.Command, args []string) {
		formatter.PromptLine("Version: %v", version.AppVersion)
		formatter.PromptLine("Build: %v (%v)", version.GitCommit, version.GitBranch)
	},
}
