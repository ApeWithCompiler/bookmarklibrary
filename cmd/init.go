package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"os"
	"path/filepath"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initialize library",
	Long:  "Initialize a new library file",
	Run: func(cmd *cobra.Command, args []string) {
		if sqlite.DatabaseExists(LibraryFile) {
			formatter.PromptLine("Can't initialize library. File allready exists")
		} else {
			path := filepath.Dir(LibraryFile)
			err := os.MkdirAll(path, os.ModePerm)
			if err != nil {

			}
			sqlite.MigrateDatabase(LibraryFile)
			formatter.PromptLine("New library initialized successfully.")
		}
	},
}
