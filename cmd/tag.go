package cmd

import (
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var tagCmd = &cobra.Command{
	Use:   "tag",
	Short: "Add tags to a link",
	Long:  "Add tags to a link",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		linkrw := sqlite.CreateLinkReaderWriter()
		linkrw.Open(LibraryFile)
		defer linkrw.Close()

		var links []model.Link

		userAll, _ := cmd.Flags().GetBool("all")
		if userAll {
			links = linkrw.ListLinks()
		} else {
			query := sqlite.NewQueryBuilder(linkrw.LinkReader)

			userIds, _ := cmd.Flags().GetStringArray("id")
			for i := range userIds {
				id, _ := strconv.Atoi(userIds[i])
				query = query.WithId(id)
			}

			userUrls, _ := cmd.Flags().GetStringArray("url")
			for i := range userUrls {
				query = query.WithWildcardUrl(userUrls[i])
			}

			userNames, _ := cmd.Flags().GetStringArray("name")
			for i := range userNames {
				query = query.WithWildcardName(userNames[i])
			}

			userTags, _ := cmd.Flags().GetStringArray("tag")
			for i := range userTags {
				query = query.WithTag(userTags[i])
			}

			links = query.QueryUnique()
		}

		for _, link := range links {
			linkrw.AddTagsFromString(link, args)
			linkrw.UpdateLink(link)
		}
	},
}
