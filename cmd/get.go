package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"sort"
	"strconv"
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get resources",
	Long:  "Get resources stored in the library",
	Run: func(cmd *cobra.Command, args []string) {
		getLinksCmd.Run(cmd, args)
	},
}

var getLinksCmd = &cobra.Command{
	Use:   "links",
	Short: "Get all links",
	Long:  "Get all links stored in the library",
	Run: func(cmd *cobra.Command, args []string) {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(LibraryFile)
		defer linkr.Close()

		var links []model.Link
		tailCnt, _ := cmd.Flags().GetInt("tail")

		if len(args) > 0 {
			for _, arg := range args {
				id, _ := strconv.Atoi(arg)
				link := linkr.GetLink(id)
				links = append(links, link)
			}
		} else {
			links = linkr.ListLinks()
		}

		if (tailCnt > -1) && (len(links) > tailCnt) {
			links = links[len(links)-tailCnt : len(links)]
		}

		message := formatter.LinkMessageList(links)
		formatter.PromptLines(message)
	},
}

var getTagsCmd = &cobra.Command{
	Use:   "tags",
	Short: "Get all tags",
	Long:  "Get all distinced stored in the library",
	Run: func(cmd *cobra.Command, args []string) {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(LibraryFile)
		defer linkr.Close()

		tags := linkr.ListTagsDistinct()
		tag_names := formatter.ReduceTags(tags)
		sort.Strings(tag_names)

		isPrettyprint, _ := cmd.Flags().GetBool("pretty")
		if isPrettyprint {
			group := formatter.GroupByFirstLetter(tag_names)
			formatter.PromptGoupMapAlphabetically(group)
		} else {
			formatter.PromptLines(tag_names)
		}

	},
}
