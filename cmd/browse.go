package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/browser"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"strconv"
)

var browseCmd = &cobra.Command{
	Use:   "browse",
	Short: "Browse links",
	Long:  "Opens a webbrowser, navigating to the urls",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(LibraryFile)
		defer linkr.Close()

		for i, _ := range args {
			if linkId, err := strconv.Atoi(args[i]); err == nil {
				link := linkr.GetLink(linkId)
				browser.OpenInDefaultBrowser(link.Url)
			} else {
				links := linkr.FindLinksLikeName(args[i])
				if len(links) > 0 {
					browser.OpenInDefaultBrowser(links[0].Url)
				}
			}

		}
	},
}
