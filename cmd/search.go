package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"sort"
)

func toWildcardTerm(term string) string {
	return "%" + term + "%"
}

func filterUniqueLinks(input []model.Link) []model.Link {
	var output []model.Link

	linksEncountered := map[uint]bool{}

	for _, link := range input {
		if linksEncountered[link.ID] != true {
			linksEncountered[link.ID] = true
			output = append(output, link)
		}
	}

	return output
}

var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "Search links",
	Long:  "Search links",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(LibraryFile)
		defer linkr.Close()

		searchExpression := args[0]

		isWildcardSearch, _ := cmd.Flags().GetBool("wildcard")
		isTagSearch, _ := cmd.Flags().GetBool("tag")
		isNameSearch, _ := cmd.Flags().GetBool("name")
		isUrlSearch, _ := cmd.Flags().GetBool("url")

		var searchResults []model.Link

		if isWildcardSearch {
			searchExpression = toWildcardTerm(searchExpression)
		}

		if isTagSearch {
			searchResults = append(searchResults, linkr.FindLinksByTag(searchExpression)...)
		}
		if isNameSearch {
			searchResults = append(searchResults, linkr.FindLinksLikeName(searchExpression)...)
		}
		if isUrlSearch {
			searchResults = append(searchResults, linkr.FindLinksLikeUrl(searchExpression)...)
		}
		/* Default: Search all attributes */
		if !isTagSearch && !isNameSearch && !isUrlSearch {
			searchResults = append(searchResults, linkr.FindLinksByTag(searchExpression)...)
			searchResults = append(searchResults, linkr.FindLinksLikeName(searchExpression)...)
			searchResults = append(searchResults, linkr.FindLinksLikeUrl(searchExpression)...)
		}

		links := filterUniqueLinks(searchResults)

		sort.Slice(links, func(i, j int) bool {
			return links[i].ID < links[j].ID
		})

		message := formatter.LinkMessageList(links)
		formatter.PromptLines(message)
	},
}
