package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"strconv"
)

var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete links",
	Long:  "Delete links",
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		linkrw := sqlite.CreateLinkReaderWriter()
		linkrw.Open(LibraryFile)
		defer linkrw.Close()

		query := sqlite.NewQueryBuilder(linkrw.LinkReader)

		userIds, _ := cmd.Flags().GetStringArray("id")
		for i := range userIds {
			id, _ := strconv.Atoi(userIds[i])
			query = query.WithId(id)
		}

		userUrls, _ := cmd.Flags().GetStringArray("url")
		for i := range userUrls {
			query = query.WithWildcardUrl(userUrls[i])
		}

		userNames, _ := cmd.Flags().GetStringArray("name")
		for i := range userNames {
			query = query.WithWildcardName(userNames[i])
		}

		userTags, _ := cmd.Flags().GetStringArray("tag")
		for i := range userTags {
			query = query.WithTag(userTags[i])
		}

		for _, link := range query.QueryUnique() {
			linkrw.DeleteLink(int(link.ID))
		}
	},
}
