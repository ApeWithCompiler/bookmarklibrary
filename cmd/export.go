package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/httpexport"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/export"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"strconv"
)

var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "Export a library",
	Long:  "Export to a JSON file",
	Run: func(cmd *cobra.Command, args []string) {
		linkr := sqlite.CreateLinkReader()
		linkr.Open(LibraryFile)
		defer linkr.Close()

		outputFile, _ := cmd.Flags().GetString("output")

		var transLibrary export.TransferLibrary

		var links []model.Link

		if len(args) > 0 {
			for _, arg := range args {
				id, _ := strconv.Atoi(arg)
				link := linkr.GetLink(id)
				links = append(links, link)
			}
		} else {
			links = linkr.ListLinks()
		}

		for i, _ := range links {
			transLink := export.TransferLink{
				Url:  links[i].Url,
				Name: links[i].Name,
			}

			for j, _ := range links[i].Tags {
				transLink.Tags = append(transLink.Tags, links[i].Tags[j].Name)
			}

			transLibrary.Links = append(transLibrary.Links, transLink)
		}

		if outputFile == "" {
			b, err := json.MarshalIndent(transLibrary, "", "  ")
			if err != nil {
				fmt.Println(err)
			}
			fmt.Print(string(b))

		} else {
			if httpexport.IsValidUrl(outputFile) {
				body := export.LinkRequestBody{
					Links: transLibrary.Links,
				}

				query, _ := json.Marshal(body)
				httpexport.Post(outputFile, query, export.LinkRequestBody{})
			} else {
				var file export.SaveFile
				file.Path = outputFile
				file.Write(transLibrary)
			}
		}

	},
}
