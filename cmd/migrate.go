package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Migrate the library",
	Long:  "Updates the schema to the new version",
	Run: func(cmd *cobra.Command, args []string) {
		if sqlite.DatabaseExists(LibraryFile) {
			formatter.PromptLine("Migrating library")
			sqlite.MigrateDatabase(LibraryFile)
		} else {
			formatter.PromptLine("Database not found: " + LibraryFile)
		}
	},
}
