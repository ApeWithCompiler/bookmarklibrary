package cmd

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/completer"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/config"
	"os"
	"os/exec"
	"strings"
)

// AppName stores the name of the app in $PATH.
// Especially needed for the spawn in 'ExecuteInteractive'.
var AppName string

// LibraryFile stores the path to the sqlite database.
var LibraryFile string

// rootCmd starts the interactive prompt if no subcommand is called.
var rootCmd = &cobra.Command{
	Use:   "bookmarks",
	Short: "Simple bookmark managing tool",
	Long: `A bookmark library written in Golang.
It has also a interactive cli with autocompletion.`,
	Run: func(cmd *cobra.Command, args []string) {
		if !executableInShellPath() {
			fmt.Println("WARNING: Executable is not found in $PATH!")
			fmt.Println("WARNING: Commands entered in the prompt will not work!")
		}

		interactiveCompleter := completer.CreateCompleter(LibraryFile)
		interactiveCli := prompt.New(
			ExecuteInteractive,
			interactiveCompleter.GetSuggestions,
			prompt.OptionTitle("bookmarklibrary - "+LibraryFile),
			prompt.OptionPrefix("> "),
			prompt.OptionCompletionWordSeparator("\\ "),
		)
		interactiveCli.Run()
	},
}

func libraryPath() string {
	env := os.Getenv("BOOKMARK_FILE")
	if env != "" {
		return env
	}

	return config.GetDefaultLibraryPath()
}

// Init initializes the command structure and its flags.
func Init() {
	rootCmd.PersistentFlags().StringVarP(&AppName, "debug", "g", "bookmarks", "Set a alternative filename/path for the interactive shell subprocess")

	rootCmd.PersistentFlags().StringVarP(&LibraryFile, "file", "f", libraryPath(), "Library file")

	rootCmd.AddCommand(getCmd)
	getCmd.AddCommand(getTagsCmd)
	getTagsCmd.Flags().BoolP("pretty", "p", false, "Prints grouped by the first letter")
	getCmd.AddCommand(getLinksCmd)
	getLinksCmd.Flags().IntP("tail", "t", -1, "Only show the last N bookmarks")

	rootCmd.AddCommand(addCmd)
	addCmd.Flags().StringP("name", "n", "", "Name for the bookmark")
	addCmd.Flags().StringArrayP("tag", "t", []string{}, "Tag for the bookmark")

	rootCmd.AddCommand(deleteCmd)
	deleteCmd.Flags().StringArrayP("id", "i", []string{}, "Specify link ids to select")
	deleteCmd.Flags().StringArrayP("url", "u", []string{}, "Specify link urls to select")
	deleteCmd.Flags().StringArrayP("name", "n", []string{}, "Specify link names to select")
	deleteCmd.Flags().StringArrayP("tag", "t", []string{}, "Specify link tags to select")

	rootCmd.AddCommand(tagCmd)
	tagCmd.Flags().BoolP("all", "x", false, "Edit all links")
	tagCmd.Flags().StringArrayP("id", "i", []string{}, "Specify link ids to select")
	tagCmd.Flags().StringArrayP("url", "u", []string{}, "Specify link urls to select")
	tagCmd.Flags().StringArrayP("name", "n", []string{}, "Specify link names to select")
	tagCmd.Flags().StringArrayP("tag", "t", []string{}, "Specify link tags to select")
	
	rootCmd.AddCommand(untagCmd)
	untagCmd.Flags().BoolP("all", "x", false, "Edit all links")
	untagCmd.Flags().StringArrayP("id", "i", []string{}, "Specify link ids to select")
	untagCmd.Flags().StringArrayP("url", "u", []string{}, "Specify link urls to select")
	untagCmd.Flags().StringArrayP("name", "n", []string{}, "Specify link names to select")
	untagCmd.Flags().StringArrayP("tag", "t", []string{}, "Specify link tags to select")

	rootCmd.AddCommand(browseCmd)

	rootCmd.AddCommand(searchCmd)
	searchCmd.Flags().BoolP("wildcard", "d", false, "Use wildcard search")
	searchCmd.Flags().BoolP("tag", "t", false, "Search in tags")
	searchCmd.Flags().BoolP("name", "n", false, "Search in names")
	searchCmd.Flags().BoolP("url", "u", false, "Search in urls")

	rootCmd.AddCommand(editCmd)
	editCmd.Flags().StringP("name", "n", "", "Name for the bookmark")
	editCmd.Flags().StringP("url", "u", "", "Url for the bookmark")

	rootCmd.AddCommand(importCmd)
	importCmd.Flags().BoolP("chrome", "c", false, "Import from chromium bookmarks")
	importCmd.Flags().StringArrayP("addtag", "a", []string{}, "Specify tags to add to every link that is merged")
	importCmd.Flags().StringArrayP("deltag", "d", []string{}, "Specify tags to remove from every link that is merged")

	rootCmd.AddCommand(exportCmd)
	exportCmd.Flags().StringP("output", "o", "", "File in wich the export will be saved. Default is stdout")

	rootCmd.AddCommand(mergeCmd)
	mergeCmd.Flags().StringP("library", "l", "", "Destination library to migrate links to")
	mergeCmd.Flags().BoolP("create", "c", false, "Create destination database before merge")
	mergeCmd.Flags().StringArrayP("id", "i", []string{}, "Specify link ids to migrate")
	mergeCmd.Flags().StringArrayP("url", "u", []string{}, "Specify link urls to migrate")
	mergeCmd.Flags().StringArrayP("name", "n", []string{}, "Specify link names to migrate")
	mergeCmd.Flags().StringArrayP("tag", "t", []string{}, "Specify link tags to migrate")
	mergeCmd.Flags().StringArrayP("addtag", "a", []string{}, "Specify tags to add to every link that is merged")
	mergeCmd.Flags().StringArrayP("deltag", "d", []string{}, "Specify tags to remove from every link that is merged")

	rootCmd.AddCommand(initCmd)
	rootCmd.AddCommand(migrateCmd)
	rootCmd.AddCommand(versionCmd)
}

// Execute executes a given command from the terminal.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// ExecuteInteractive executes a given command from within the interactive shell.
// To provide the command to cobra it spawns a shell subprocess,
// which emulates as if the command was called from the terminal.
func ExecuteInteractive(line string) {
	line = strings.TrimSpace(line)
	if line == "" {
		return
	} else if line == "quit" || line == "exit" {
		fmt.Println("Exiting.")
		os.Exit(0)
		return
	}

	shellCmd := fmt.Sprintf("%v %v -f %v", AppName, line, LibraryFile)
	subProccess := exec.Command("/bin/sh", "-c", shellCmd)
	subProccess.Stdin = os.Stdin
	subProccess.Stdout = os.Stdout
	subProccess.Stderr = os.Stderr
	if err := subProccess.Run(); err != nil {
		fmt.Printf("Got error: %s\n", err.Error())
	}
	return
}

// executableInShellPath checks if the app can be found in $PATH.
func executableInShellPath() bool {
	shellCmd := fmt.Sprintf("which %v", AppName)
	subProccess := exec.Command("/bin/sh", "-c", shellCmd)
	if err := subProccess.Run(); err != nil {
		return false
	}
	return true
}
