package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/browser/chromium"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/export"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

func argsOrDefault(args []string, def string) []string {
	if len(args) > 0 {
		return args
	}

	return []string{def,}
}

func linkFromExport(link export.TransferLink) model.Link {
	l := model.CreateLink(link.Url, link.Name)
	for _, tag := range link.Tags {
		l.Tags = append(l.Tags, model.CreateTag(tag))
	}

	return l
}

var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Import a library",
	Long:  "Import from a JSON file",
	Run: func(cmd *cobra.Command, args []string) {
		linkrw := sqlite.CreateLinkReaderWriter()
		linkrw.Open(LibraryFile)
		defer linkrw.Close()

		userChrome, _ := cmd.Flags().GetBool("chrome")

		userAddTag, _ := cmd.Flags().GetStringArray("addtag")
		userDelTag, _ := cmd.Flags().GetStringArray("deltag")

		var links []model.Link
		
		if userChrome {
			for _, arg := range argsOrDefault(args, chromium.GetLinuxDefaultFilePath()) {
				file := chromium.Read(arg)
				links = append(links, chromium.GetLinks(file)...)
			}
		} else {
			for _, arg := range args {
				var file export.SaveFile
				file.Path = arg

				importLibrary := file.Read()
				for i := range importLibrary.Links {
					links = append(links, linkFromExport(importLibrary.Links[i]))
				}
			}
		}

		for _, link := range links {
			if len(linkrw.FindLinksByUrl(link.Url)) == 0 {
				id := linkrw.AddLink(link)
				l := linkrw.GetLink(int(id))

				for _, tag := range link.Tags {
					linkrw.AddTag(l, tag)
				}

				for _, tag := range userDelTag {
					linkrw.DeleteTagFromString(l, tag)
				}

				for _, tag := range userAddTag {
					linkrw.AddTagFromString(l, tag)
				}
			}
		}
	},
}
