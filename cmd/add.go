package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/scraper"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/textsanitizer"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add a link",
	Long:  "Add a link",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		linkrw := sqlite.CreateLinkReaderWriter()
		linkrw.Open(LibraryFile)
		defer linkrw.Close()

		var url string
		name, _ := cmd.Flags().GetString("name")
		userTags, _ := cmd.Flags().GetStringArray("tag")

		url = args[0]

		if len(linkrw.FindLinksByUrl(url)) > 0 {
			formatter.PromptLine("Cant insert, URL allready exists!")
			return
		}

		if name == "@title" {
			page := scraper.ScrapePage(url)
			name = textsanitizer.Sanitize(page.GetTitle())
		}

		link := model.CreateLink(url, name)
		id := linkrw.AddLink(link)

		link = linkrw.GetLink(int(id))
		linkrw.AddTagsFromString(link, userTags)

	},
}
