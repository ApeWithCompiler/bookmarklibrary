package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/scraper"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/addin/textsanitizer"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
	"strconv"
)

var editCmd = &cobra.Command{
	Use:   "edit",
	Short: "Edit links",
	Long:  "Edit links",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		linkrw := sqlite.CreateLinkReaderWriter()
		linkrw.Open(LibraryFile)
		defer linkrw.Close()

		name, _ := cmd.Flags().GetString("name")
		url, _ := cmd.Flags().GetString("url")

		linkId, _ := strconv.Atoi(args[0])
		link := linkrw.GetLink(linkId)

		if name != "" {
			if name == "@title" {
				page := scraper.ScrapePage(link.Url)
				name = textsanitizer.Sanitize(page.GetTitle())
			}
			link.SetName(name)
			linkrw.UpdateLink(link)
		}

		if url != "" {

			if len(linkrw.FindLinksByUrl(url)) > 0 {
				formatter.PromptLine("Cant insert, URL allready exists!")
				return
			}

			link.SetUrl(url)
			linkrw.UpdateLink(link)
		}
	},
}
