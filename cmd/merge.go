package cmd

import (
	"os"
	"path/filepath"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/formatter"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/model"
	"gitlab.com/ApeWithCompiler/bookmarklibrary/internal/sqlite"
)

var mergeCmd = &cobra.Command{
	Use:   "merge",
	Short: "Merge libraries",
	Long:  "Merge bookmarks from one library to another",
	Run: func(cmd *cobra.Command, args []string) {
		srcLibrary := sqlite.CreateLinkReader()
		srcLibrary.Open(LibraryFile)
		defer srcLibrary.Close()

		destFile, _ := cmd.Flags().GetString("library")
		
		create, _ := cmd.Flags().GetBool("create")
		if create {
			if !sqlite.DatabaseExists(destFile) {
				path := filepath.Dir(destFile)
				err := os.MkdirAll(path, os.ModePerm)
				if err != nil {

				}
				sqlite.MigrateDatabase(destFile)
			}
		}

		if !sqlite.DatabaseExists(destFile) {
			formatter.PromptLine("Target database not exsisting")
			formatter.PromptLine("Consider -c for creation before merge")
			return
		}


		destLibrary := sqlite.CreateLinkReaderWriter()
		destLibrary.Open(destFile)
		defer destLibrary.Close()

		userIds, _ := cmd.Flags().GetStringArray("id")
		userUrls, _ := cmd.Flags().GetStringArray("url")
		userNames, _ := cmd.Flags().GetStringArray("name")
		userTags, _ := cmd.Flags().GetStringArray("tag")

		userAddTag, _ := cmd.Flags().GetStringArray("addtag")
		userDelTag, _ := cmd.Flags().GetStringArray("deltag")

		var links []model.Link

		if len(userIds) > 0 || len(userUrls) > 0 || len(userNames) > 0 || len(userTags) > 0 {
			query := sqlite.NewQueryBuilder(srcLibrary)

			userIds, _ := cmd.Flags().GetStringArray("id")
			for i := range userIds {
				id, _ := strconv.Atoi(userIds[i])
				query = query.WithId(id)
			}

			userUrls, _ := cmd.Flags().GetStringArray("url")
			for i := range userUrls {
				query = query.WithWildcardUrl(userUrls[i])
			}

			userNames, _ := cmd.Flags().GetStringArray("name")
			for i := range userNames {
				query = query.WithWildcardName(userNames[i])
			}

			userTags, _ := cmd.Flags().GetStringArray("tag")
			for i := range userTags {
				query = query.WithTag(userTags[i])
			}

			links = query.QueryUnique()
		} else {
			links = srcLibrary.ListLinks()
		}

		for _, link := range filterUniqueLinks(links) {
			if len(destLibrary.FindLinksByUrl(link.Url)) == 0 {
				link.ID = 0
				id := destLibrary.AddLink(link)
				l := destLibrary.GetLink(int(id))

				for _, tag := range link.Tags {
					destLibrary.AddTag(l, tag)
				}

				for _, tag := range userDelTag {
					destLibrary.DeleteTagFromString(l, tag)
				}

				for _, tag := range userAddTag {
					destLibrary.AddTagFromString(l, tag)
				}
			}
		}
	},
}
